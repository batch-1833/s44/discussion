//fetch() method in JavaScript is used to send request in the server and load the recieve response in the webpages. The request and response is in JSON format.

//Syntax:
	// fetch('url', options)
			// url - thos os the url which the request is to be made(endpoint).
			//options - an array of properties that contains the HTTP method, body of request, headers.

//Get post data
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
//invoke the showPosts() // this will dispay all the information
.then((data) => showPosts(data));

//Add post data
document.querySelector("#form-add-post").addEventListener("submit", (e) =>{

	// We can prevent the page from loading
	e.preventDefault();
	fetch("https://jsonplaceholder.typicode.com/posts",{
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers:{
			"Content-Type": "application/json"
		}
	})
	.then((response) => response.json())
	.then((data) =>{
		console.log(data);
		alert("Successfully Added!");
	})
	
	//Resets the state of our inputs into blacks after submitting a new post.
	document.querySelector("#txt-title").value = null;
	document.querySelector("#txt-body").value = null;
	
})


// View Posts
const showPosts = (posts) =>{
	let postEntries = "";

	// We will used forEach() to display each movie inside our mock database.
	posts.forEach((post) =>{
		postEntries += `
					<div id="post-${post.id}">
						<h3 id="post-title-${post.id}">${post.title}</h3>
						<p id="post-body-${post.id}">${post.body}</p>
						<button onclick="editPost('${post.id}')">Edit</button>
						<button onclick="deletePost('${post.id}')">Delete</button>
					</div>
				`
	});

	//console.log(postEntries);

	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

//Edit Post Button

const editPost = (id) =>{
	//Contain the value of the title and body in a variable.
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// Pass the id, title and body of the movie post to be updated in the edit post/form.
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	//To remove the disable property
	document.querySelector("#btn-submit-update").removeAttribute("disabled");
}

//Update post.
document.querySelector("#form-edit-post").addEventListener("submit", (e) =>{
	e.preventDefault();
	let id = document.querySelector("#txt-edit-id").value

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: "PUT",
		body: JSON.stringify({
			id: id,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers:{
			"Content-Type" : "application/json"
		}
	})
	.then(response => response.json())
	.then(data =>{
		console.log(data);
		alert("Successfully updated");

		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;

		//Add a attribute in a HTML element
		document.querySelector("#btn-submit-update").setAttribute("disabled", true);
	})
	
})
